function ltrim(str) {
    if (!str) return str;
    return str.replace(/^\s+/g, '');
}

function linesplit(str) {
    return str.split(/\r?\n/);
}

function adjustsection(str) {
    var newstr = linesplit(str);
    var sections = newstr.map(element => {
        var trimmed = ltrim(element);
        return trimmed += '\n';
    });
    var spliced = sections.splice(0, 1);
    return sections.join("");
}

$("section").each(function(index) {
    var newText = adjustsection($(this).contents().filter(function(){ return this.nodeType == 3; }).first().text());
    $(this).contents().filter(function(){ return this.nodeType == 3; }).first().replaceWith(marked(newText));
});